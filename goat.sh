apt-get update -y
apt-get upgrade -y
apt install docker.io -y
systemctl stop ufw
systemctl disable ufw
docker pull webgoat/webgoat-8.0
docker run -p 8080:8080 -t webgoat/webgoat-8.0
